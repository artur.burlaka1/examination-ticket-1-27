package edu.hneu.mjt.burlaka.ticket_1_27.dto;

import lombok.Data;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@Data
public class TrainerDTO {

    private Long id;

    @NotBlank(message = "Ім'я тренера не може бути порожнім")
    private String name;

    @NotBlank(message = "Вид спорту не може бути порожнім")
    private String sportType;

    private Integer rating;

    @NotBlank(message = "Email тренера не може бути порожнім")
    @Email(regexp = "^[A-Za-z0-9+_.-]+@[a-zA-Z0-9.-]+$")
    private String email;
}