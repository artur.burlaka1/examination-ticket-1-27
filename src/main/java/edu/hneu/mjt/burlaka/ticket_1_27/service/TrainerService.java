package edu.hneu.mjt.burlaka.ticket_1_27.service;

import edu.hneu.mjt.burlaka.ticket_1_27.dto.TrainerDTO;
import edu.hneu.mjt.burlaka.ticket_1_27.entity.Trainer;
import edu.hneu.mjt.burlaka.ticket_1_27.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TrainerService {

    @Autowired
    private TrainerRepository trainerRepository;

    // Метод для створення нового тренера
    public TrainerDTO createTrainer(TrainerDTO trainerDTO) {
        Trainer trainer = convertToEntity(trainerDTO); // Перетворення DTO в сутність
        Trainer savedTrainer = trainerRepository.save(trainer);
        return convertToDTO(savedTrainer); // Перетворення сутності в DTO
    }

    // Метод для отримання всіх тренерів
    public List<TrainerDTO> getAllTrainers() {
        List<Trainer> trainers = (List<Trainer>) trainerRepository.findAll();
        return trainers.stream()
                .map(this::convertToDTO) // Перетворення сутностей в DTO
                .collect(Collectors.toList());
    }

    // Метод для отримання тренера за ID
    public TrainerDTO getTrainerById(Long id) {
        Optional<Trainer> trainer = trainerRepository.findById(id);
        return trainer.map(this::convertToDTO) // Перетворення сутності в DTO
                .orElse(null); // Повернення null, якщо тренера не знайдено
    }

    // Метод для оновлення тренера
    public TrainerDTO updateTrainer(Long id, TrainerDTO trainerDTO) {
        Optional<Trainer> existingTrainer = trainerRepository.findById(id);
        if (existingTrainer.isPresent()) {
            Trainer trainer = existingTrainer.get();
            trainer.setName(trainerDTO.getName());
            trainer.setSportType(trainerDTO.getSportType());
            trainer.setRating(trainerDTO.getRating());
            trainer.setEmail(trainerDTO.getEmail());
            Trainer updatedTrainer = trainerRepository.save(trainer);
            return convertToDTO(updatedTrainer);
        } else {
            return null; // Повернення null, якщо тренера не знайдено
        }
    }

    // Метод для видалення тренера
    public void deleteTrainer(Long id) {
        trainerRepository.deleteById(id);
    }

    // Метод для перетворення Trainer в TrainerDTO
    private TrainerDTO convertToDTO(Trainer trainer) {
        TrainerDTO trainerDTO = new TrainerDTO();
        trainerDTO.setId(trainer.getId());
        trainerDTO.setName(trainer.getName());
        trainerDTO.setSportType(trainer.getSportType());
        trainerDTO.setRating(trainer.getRating());
        trainerDTO.setEmail(trainer.getEmail());
        return trainerDTO;
    }

    // Метод для перетворення TrainerDTO в Trainer
    private Trainer convertToEntity(TrainerDTO trainerDTO) {
        Trainer trainer = new Trainer();
        trainer.setName(trainerDTO.getName());
        trainer.setSportType(trainerDTO.getSportType());
        trainer.setRating(trainerDTO.getRating());
        trainer.setEmail(trainerDTO.getEmail());
        return trainer;
    }
}
