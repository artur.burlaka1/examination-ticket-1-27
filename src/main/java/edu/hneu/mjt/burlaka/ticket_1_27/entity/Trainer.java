package edu.hneu.mjt.burlaka.ticket_1_27.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Trainer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Ім'я тренера не може бути порожнім")
    @Column(nullable = false)
    private String name;

    @NotBlank(message = "Вид спорту не може бути порожнім")
    @Column(nullable = false)
    private String sportType;

    @Column(nullable = false)
    private Integer rating;

    @NotBlank(message = "Email тренера не може бути порожнім")
    @Column(nullable = false, unique = true)
    @Email(regexp = "^[A-Za-z0-9+_.-]+@hneu.net$")
    private String email;

    @OneToMany(mappedBy = "trainer", cascade = CascadeType.ALL)
    private List<TrainingSession> trainingSessions = new ArrayList<>();
}
