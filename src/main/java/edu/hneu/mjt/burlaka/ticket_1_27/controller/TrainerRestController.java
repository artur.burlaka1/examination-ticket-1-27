package edu.hneu.mjt.burlaka.ticket_1_27.controller;

import edu.hneu.mjt.burlaka.ticket_1_27.dto.TrainerDTO;
import edu.hneu.mjt.burlaka.ticket_1_27.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import  jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/trainers")
public class TrainerRestController {

    @Autowired
    private TrainerService trainerService;

    @GetMapping
    public List<TrainerDTO> getAllTrainers() {
        return trainerService.getAllTrainers();
    }

    @PostMapping
    public ResponseEntity<String> addTrainer(@Valid @RequestBody TrainerDTO trainerDTO, BindingResult result) {
        if (validateTrainer(trainerDTO, result)) {
            return ResponseEntity.badRequest().body(result.getAllErrors().toString());
        }
        trainerService.createTrainer(trainerDTO);
        return ResponseEntity.ok("Trainer created successfully");
    }

    @GetMapping("/{id}")
    public TrainerDTO getTrainerById(@PathVariable Long id) {
        return trainerService.getTrainerById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateTrainer(@PathVariable Long id, @Valid @RequestBody TrainerDTO trainerDTO, BindingResult result) {
        if (validateTrainer(trainerDTO, result)) {
            return ResponseEntity.badRequest().body(result.getAllErrors().toString());
        }
        trainerService.updateTrainer(id, trainerDTO);
        return ResponseEntity.ok("Trainer updated successfully");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTrainer(@PathVariable Long id) {
        trainerService.deleteTrainer(id);
        return ResponseEntity.ok("Trainer deleted successfully");
    }

    // Метод для валідації даних тренера
    private boolean validateTrainer(TrainerDTO trainerDTO, BindingResult result) {
        boolean hasErrors = false;

        if (trainerDTO.getName() == null || trainerDTO.getName().isEmpty()) {
            result.rejectValue("name", "error.name.required", "Ім'я тренера не може бути порожнім");
            hasErrors = true;
        }
        if (trainerDTO.getSportType() == null || trainerDTO.getSportType().isEmpty()) {
            result.rejectValue("sportType", "error.sportType.required", "Вид спорту не може бути порожнім");
            hasErrors = true;
        }
        if (trainerDTO.getRating() == null || trainerDTO.getRating() < 0) {
            result.rejectValue("rating", "error.rating.required", "Рейтинг тренера повинен бути невід'ємним числом");
            hasErrors = true;
        }
        if (trainerDTO.getEmail() == null || trainerDTO.getEmail().isEmpty()) {
            result.rejectValue("email", "error.email.required", "Електронна пошта тренера не може бути порожньою");
            hasErrors = true;
        }
        if (!trainerDTO.getEmail().matches("^[A-Za-z0-9+_.-]+@[a-zA-Z0-9.-]+$")) {
            result.rejectValue("email", "error.email.format", "Неправильний формат електронної пошти");
            hasErrors = true;
        }
        return hasErrors;
    }
}
