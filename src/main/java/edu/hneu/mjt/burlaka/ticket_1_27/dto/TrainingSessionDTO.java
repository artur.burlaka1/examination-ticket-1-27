package edu.hneu.mjt.burlaka.ticket_1_27.dto;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

@Data
public class TrainingSessionDTO {

    private Long id;

    @NotBlank(message = "Вид спорту не може бути порожнім")
    private String sportType;

    @Positive(message = "Тривалість заняття повинна бути позитивним числом")
    private Integer duration;

    @Positive(message = "Вартість заняття повинна бути позитивним числом")
    private Double cost;

    private TrainerDTO trainer;
}
