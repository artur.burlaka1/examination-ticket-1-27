package edu.hneu.mjt.burlaka.ticket_1_27.repository;

import edu.hneu.mjt.burlaka.ticket_1_27.entity.TrainingSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingSessionRepository extends JpaRepository<TrainingSession, Long> {
}
