package edu.hneu.mjt.burlaka.ticket_1_27.service;
import edu.hneu.mjt.burlaka.ticket_1_27.dto.TrainerDTO;
import edu.hneu.mjt.burlaka.ticket_1_27.dto.TrainingSessionDTO;
import edu.hneu.mjt.burlaka.ticket_1_27.entity.Trainer;
import edu.hneu.mjt.burlaka.ticket_1_27.entity.TrainingSession;
import edu.hneu.mjt.burlaka.ticket_1_27.repository.TrainerRepository;
import edu.hneu.mjt.burlaka.ticket_1_27.repository.TrainingSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TrainingSessionService {

    @Autowired
    private TrainingSessionRepository trainingSessionRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    // Метод для створення нового заняття тренера
    public TrainingSessionDTO createTrainingSession(TrainingSessionDTO trainingSessionDTO) {
        TrainingSession trainingSession = convertToEntity(trainingSessionDTO); // Перетворення DTO в сутність
        TrainingSession savedTrainingSession = trainingSessionRepository.save(trainingSession);
        return convertToDTO(savedTrainingSession); // Перетворення сутності в DTO
    }

    // Метод для отримання всіх занять тренера
    public List<TrainingSessionDTO> getAllTrainingSessions() {
        List<TrainingSession> trainingSessions = (List<TrainingSession>) trainingSessionRepository.findAll();
        return trainingSessions.stream()
                .map(this::convertToDTO) // Перетворення сутностей в DTO
                .collect(Collectors.toList());
    }

    // Метод для отримання заняття тренера за ID
    public TrainingSessionDTO getTrainingSessionById(Long id) {
        Optional<TrainingSession> trainingSession = trainingSessionRepository.findById(id);
        return trainingSession.map(this::convertToDTO) // Перетворення сутності в DTO
                .orElse(null); // Повернення null, якщо заняття не знайдено
    }

    // Метод для оновлення заняття тренера
    public TrainingSessionDTO updateTrainingSession(Long id, TrainingSessionDTO trainingSessionDTO) {
        Optional<TrainingSession> existingTrainingSession = trainingSessionRepository.findById(id);
        if (existingTrainingSession.isPresent()) {
            TrainingSession trainingSession = existingTrainingSession.get();
            trainingSession.setSportType(trainingSessionDTO.getSportType());
            trainingSession.setDuration(trainingSessionDTO.getDuration());
            trainingSession.setCost(trainingSessionDTO.getCost());

            // Оновлення зв'язку з тренером
            Optional<Trainer> trainer = trainerRepository.findById(trainingSessionDTO.getTrainer().getId());
            if (trainer.isPresent()) {
                trainingSession.setTrainer(trainer.get());
            }

            TrainingSession updatedTrainingSession = trainingSessionRepository.save(trainingSession);
            return convertToDTO(updatedTrainingSession); // Перетворення сутності в DTO
        } else {
            return null; // Повернення null, якщо заняття не знайдено
        }
    }

    // Метод для видалення заняття тренера
    public void deleteTrainingSession(Long id) {
        trainingSessionRepository.deleteById(id);
    }

    // Метод для перетворення TrainingSession в TrainingSessionDTO
    private TrainingSessionDTO convertToDTO(TrainingSession trainingSession) {
        TrainingSessionDTO trainingSessionDTO = new TrainingSessionDTO();
        trainingSessionDTO.setId(trainingSession.getId());
        trainingSessionDTO.setSportType(trainingSession.getSportType());
        trainingSessionDTO.setDuration(trainingSession.getDuration());
        trainingSessionDTO.setCost(trainingSession.getCost());

        // Перетворення Trainer в TrainerDTO
        TrainerDTO trainerDTO = new TrainerDTO();
        trainerDTO.setId(trainingSession.getTrainer().getId());
        trainerDTO.setName(trainingSession.getTrainer().getName());
        trainerDTO.setSportType(trainingSession.getTrainer().getSportType());
        trainerDTO.setRating(trainingSession.getTrainer().getRating());
        trainerDTO.setEmail(trainingSession.getTrainer().getEmail());
        trainingSessionDTO.setTrainer(trainerDTO);

        return trainingSessionDTO;
    }

    private TrainingSession convertToEntity(TrainingSessionDTO trainingSessionDTO) {
        TrainingSession trainingSession = new TrainingSession();
        trainingSession.setSportType(trainingSessionDTO.getSportType());
        trainingSession.setDuration(trainingSessionDTO.getDuration());
        trainingSession.setCost(trainingSessionDTO.getCost());

        // Зв'язування тренера
        Optional<Trainer> trainer = trainerRepository.findById(trainingSessionDTO.getTrainer().getId());
        if (trainer.isPresent()) {
            trainingSession.setTrainer(trainer.get());
        }

        return trainingSession;
    }
}
