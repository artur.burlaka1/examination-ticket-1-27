package edu.hneu.mjt.burlaka.ticket_1_27.entity;
import jakarta.persistence.*;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

@Entity
@Data
public class TrainingSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Вид спорту не може бути порожнім")
    @Column(nullable = false)
    private String sportType;

    @Positive(message = "Тривалість заняття повинна бути позитивним числом")
    @Column(nullable = false)
    private Integer duration; // Тривалість в хвилинах

    @Positive(message = "Вартість заняття повинна бути позитивним числом")
    @Column(nullable = false)
    private Double cost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trainer_id", nullable = false)
    private Trainer trainer;
}
