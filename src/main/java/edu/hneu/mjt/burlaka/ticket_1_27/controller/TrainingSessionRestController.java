package edu.hneu.mjt.burlaka.ticket_1_27.controller;
import edu.hneu.mjt.burlaka.ticket_1_27.dto.TrainingSessionDTO;
import edu.hneu.mjt.burlaka.ticket_1_27.service.TrainingSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/trainingSessions")
public class TrainingSessionRestController {

    @Autowired
    private TrainingSessionService trainingSessionService;

    @GetMapping
    public List<TrainingSessionDTO> getAllTrainingSessions() {
        return trainingSessionService.getAllTrainingSessions();
    }

    @PostMapping
    public ResponseEntity<String> addTrainingSession(@Valid @RequestBody TrainingSessionDTO trainingSessionDTO, BindingResult result) {
        if (validateTrainingSession(trainingSessionDTO, result)) {
            return ResponseEntity.badRequest().body(result.getAllErrors().toString());
        }
        trainingSessionService.createTrainingSession(trainingSessionDTO);
        return ResponseEntity.ok("Training session created successfully");
    }

    @GetMapping("/{id}")
    public TrainingSessionDTO getTrainingSessionById(@PathVariable Long id) {
        return trainingSessionService.getTrainingSessionById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateTrainingSession(@PathVariable Long id, @Valid @RequestBody TrainingSessionDTO trainingSessionDTO, BindingResult result) {
        if (validateTrainingSession(trainingSessionDTO, result)) {
            return ResponseEntity.badRequest().body(result.getAllErrors().toString());
        }
        trainingSessionService.updateTrainingSession(id, trainingSessionDTO);
        return ResponseEntity.ok("Training session updated successfully");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTrainingSession(@PathVariable Long id) {
        trainingSessionService.deleteTrainingSession(id);
        return ResponseEntity.ok("Training session deleted successfully");
    }

    // Валідація для TrainingSessionDTO
    private boolean validateTrainingSession(TrainingSessionDTO trainingSessionDTO, BindingResult result) {
        boolean hasErrors = false;

        if (trainingSessionDTO.getSportType() == null || trainingSessionDTO.getSportType().isEmpty()) {
            result.rejectValue("sportType", "error.sportType.required", "Вид спорту не може бути порожнім");
            hasErrors = true;
        }
        if (trainingSessionDTO.getDuration() == null || trainingSessionDTO.getDuration() <= 0) {
            result.rejectValue("duration", "error.duration.required", "Тривалість заняття повинна бути позитивним числом");
            hasErrors = true;
        }
        if (trainingSessionDTO.getCost() == null || trainingSessionDTO.getCost() <= 0) {
            result.rejectValue("cost", "error.cost.required", "Вартість заняття повинна бути позитивним числом");
            hasErrors = true;
        }
        if (trainingSessionDTO.getTrainer() == null) {
            result.rejectValue("trainerId", "error.trainerId.required", "Виберіть тренера");
            hasErrors = true;
        }

        return hasErrors;
    }
}
