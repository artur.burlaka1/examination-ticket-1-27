package edu.hneu.mjt.burlaka.ticket_1_27;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExaminationTicket127Application {

    public static void main(String[] args) {
        SpringApplication.run(ExaminationTicket127Application.class, args);
    }

}
